# Performance and productivity evaluation of Kokkos

The report 'Performance and productivity evaluation of Kokkos' was written as part of a final project of the lecture 'Design of Parallel and High-Performance Computing' at ETH Zurich. The reported results were produced with the implementations included in this repository.

## Project structure

The `deriche` and `lu` directories contain the actual code implementations. The Python scripts with which the shown figures were generated can be found in the `statistics` directory. `measurements` and `figures` contain the actual measurements and their corresponding generated figures, respectively. Finally, the `euler.sh` script can be used for convenience to build and run the implementations on ETH's Euler cluster. However, note that one needs to be an ETH member in order to use the cluster and be inside the ETH network, e.g. via VPN.

## Building and running

All codes were compiled using GCC version 6.3.0 using the -O3 optimization flag. We used Open MPI 3.0.0 and Kokkos 3.3.1. The CUDA version used was 11.2.

In order to build and run the experiments, clone the correct Kokkos version:

```bash
git clone https://github.com/kokkos/kokkos.git
git -C ./kokkos/ checkout 4d23839
```

Ensure the other used libraries mentioned above are set to the correct version, too. Then, `cd` to the desired implementation directory, that is, either `lu` or `deriche`. Set the correct config parameters in `compile_run_config.json` resp. `compile_run_config_gpu.json` (see the other predefined configs for examples on how to set the parameters correctly).

We can then build and run the non-GPU experiments by executing:

```bash
python3 compile_run.py clean .
python3 compile_run.py build . $KOKKOS_DIR
python3 compile_run.py run .
```

Note that `$KOKKOS_DIR` is set to the relative path to the cloned Kokkos repository.

Similarly, the GPU-related experiments can be built and run by executing:

```bash
python3 compile_run_gpu.py clean .
python3 compile_run_gpu.py build . $KOKKOS_DIR
python3 compile_run_gpu.py run .
```